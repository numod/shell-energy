// This file is part of GOAST, a C++ library for variational methods in Geometry Processing
//
// Copyright (C) 2021 Behrend Heeren & Josua Sassen, University of Bonn <goast@ins.uni-bonn.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
#include <iostream>
#include <fstream>
#include <iomanip>
#include <queue>
#include <set>

// Libigl includes
#include <igl/C_STR.h>
#include <igl/matlab/mexErrMsgTxt.h>
#include <igl/matlab/MexStream.h>
#include <igl/matlab/parse_rhs.h>
#include <igl/matlab/prepare_lhs.h>
#include <igl/matlab/validate_arg.h>

// Shell includes
#include <shell-energy/shell_energy.h>


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ) {
  using namespace igl;
  using namespace igl::matlab;
  igl::matlab::MexStream mout;
  std::streambuf *outbuf = std::cout.rdbuf( &mout );

  // Input
  mexErrMsgTxt( nrhs == 8, "nrhs should be == 8" );

  Eigen::MatrixXd V_def, V_undef;
  Eigen::MatrixXi F, E, EF, EI;
  Eigen::VectorXi EMAP;
  double bendingWeight;

  parse_rhs_double( prhs, V_undef );
  parse_rhs_double( prhs + 1, V_def );
  parse_rhs_index( prhs + 2, F );
  parse_rhs_index( prhs + 3, E );
  parse_rhs_index( prhs + 4, EMAP );
  parse_rhs_index( prhs + 5, EF );
  parse_rhs_index( prhs + 6, EI );
  bendingWeight = *mxGetDoubles( prhs[7] );

  mexErrMsgTxt( V_def.cols() == 3, "V_def must be #V by 3" );
  mexErrMsgTxt( V_undef.cols() == 3, "V_undef must be #V by 3" );
  mexErrMsgTxt( F.cols() == 3, "F must be #F by 3" );
  mexErrMsgTxt( E.cols() == 2, "E must be #E by 3" );
  mexErrMsgTxt( EI.cols() == 2, "EI must be #E by 2" );
  mexErrMsgTxt( EF.cols() == 2, "EF must be #E by 2" );
  mexErrMsgTxt( EMAP.size() == F.rows() * 3, "EMAP must be #F * 3" );

  // Compute
  double out = shell::shell_energy( V_undef, V_def, F, E, EMAP, EF, EI, bendingWeight );

  // Output
  if ( nlhs == 1 ) {
    plhs[0] = mxCreateDoubleScalar( out );
  }

  std::cout.rdbuf( outbuf );
}